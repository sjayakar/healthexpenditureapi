'use strict';

var config = require('../config/configuration');

var mongo_connectionstring = 'mongodb://' + config.dbconfig.mongo_url + ':' + config.dbconfig.mongo_port + '/' + config.dbconfig.mongo_db

module.exports = mongo_connectionstring;