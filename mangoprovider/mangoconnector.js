'use strict';

var connectionstring = require('../connectionstring'); 

var dbchannel = mongoose.connect(connectionstring);

module.exports = dbchannel; 