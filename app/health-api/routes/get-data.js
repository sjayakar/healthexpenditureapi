'use strict';

var Data = require('../models/expenditure');
var remove = require('../lib/rmv-key-value-json')
var columns = '1980 1981 1982 1983 1984 1985 1986 1987 1988 1989 1990 1991 1992 1993 1994 1995 1996 1997 1998 1999 2000 2001 2002 2003 2004 2005 2006 2007 2008 2009';
var get = function (req, res) {
	var param = req.query;
	//console.log(param);
	var query = Data.find({});

	if(param != [])
	{
		query.where('Code',param.type);
		console.log(param.type);
		//console.log(param.region === undefined);
		if(param.region === undefined && param.state === undefined)
		{
			console.log('National');
			query.where('Group','United States');
			query.where('Region_Name','United States');	
		}
		else if(!param.region)
		{
			console.log('state');
			query.where('Group','State');
			query.where('State_Name',param.state);
		}
		else if(!param.state)
		{			
			console.log('Region');
			query.where('Group','Region');
			query.where('Region_Name',param.region);
		}
		else
		{
			query.where('Group','United States');
			query.where('Region_Name','United States');
			console.log('national data');
		}
	}
	else
	{
		//console.log(req.query); 
	}

  query.select(columns)
  .exec(function (err, record) {    	
      if (err) return;
     //var result = remove('_id', record);
     //console.log(record[0]);
     res.json(record[0]);
      
    });
};

module.exports = get;