'use strict';

var Data = require('../models/expenditure');
var get = function (req, res) {
  Data.where('Code',1)
  .where('Group','State')
  .select('State_Name')
    .exec(function (err, record) {    	
      if (err) return;
      res.json(record);
    });
};

module.exports = get;