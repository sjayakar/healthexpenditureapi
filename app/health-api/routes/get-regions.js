'use strict';

var Data = require('../models/expenditure');
var get = function (req, res) {
  Data.where('Code',1)
  .where('Group','Region')
  .select('Region_Name Region_Number')
    .exec(function (err, record) {    	
      if (err) return;
      res.json(record);
    });
};

module.exports = get;