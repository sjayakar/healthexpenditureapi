'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//need to update the schema
var ExpenditureSchema   = new Schema({  
  Code: Number,
  Item: String,
  Group: String,
  Region_Number: Number,
  Region_Name: String,
  State_Name: String,
  1980: Number,
  1981: Number,
  1982: Number,
  1983: Number,
  1984: Number,
  1985: Number,
  1986: Number,
  1987: Number,
  1988: Number,
  1989: Number,
  1990: Number,
  1991: Number,
  1992: Number,
  1993: Number,
  1994: Number,
  1995: Number,
  1996: Number,
  1997: Number,
  1998: Number,
  1999: Number,
  2000: Number,
  2001: Number,
  2002: Number,
  2003: Number,
  2004: Number,
  2005: Number,
  2006: Number,
  2007: Number,
  2008: Number,
  2009: Number,
  Average_Annual_Percent_Growth:Number
});

module.exports = mongoose.model('Expenditure', ExpenditureSchema);