'use strict';

var remove = function (key,data){	
	for(var i = 0; i < data.length; i++) {
    delete data[i][key];
    return data;
	}
};

module.exports = remove;