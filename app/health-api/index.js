'use strict';

var express = require('express');
var router = express.Router();

var gettypes = require('./routes/get-types');
var getStates = require('./routes/get-states');
var getRegions = require('./routes/get-regions');
var getData = require('./routes/get-data');
var config = require('../../config/configuration');
var defaultPath = '/health-api';

var route = function (path) {	
  var apiPath = path || defaultPath;
  router.route(apiPath).get(gettypes);
  router.route(apiPath + config.routes.api_url_states).get(getStates);
  router.route(apiPath + config.routes.api_url_regions).get(getRegions); 
  router.route(apiPath + config.routes.api_url_data).get(getData);
  return router;
};

module.exports = route;