'use strict';

var express = require('express');
var mongoose = require('mongoose');
//var expressListRoutes = require('express-list-routes');
var app = express();
var router = express.Router();

var config = require('./config/configuration');
var enableCors = require('./config/enable-verbs');
var connectionstring = require('./mangoprovider/connectionstring');

var apis = {  
  bootcamp: require('./app/health-api')(config.routes.api_url_health)  
};

app.use(enableCors);

mongoose.connect(connectionstring);

router.get('/', function (req, res) {	
  res.json({ message: config.welcome_msg });
});
app.use('/', apis.bootcamp);
app.use('/', router);

app.listen(config.port);
console.log(config.host);

/*
_.forEach(apis, function (api, fn) {
  expressListRoutes({prefix: ''}, fn, api);
});*/