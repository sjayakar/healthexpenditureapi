'use strict';

var config = require('./config/configuration'),
		readcsvdata = require('./loaddata/csv-data-reader'),
		csvtojson = require('./loaddata/csv-to-json');

var config = require('./Config/configuration');
// Data
//console.log(config.csvfilepath);
var csvData = readcsvdata(config.csvfilepath);
var jsonData = csvtojson(csvData);
var data = JSON.parse(jsonData, 'utf8');
 
module.exports = data;