'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var StateSchema = new Schema({
  State_Name: String  
});

module.exports = mongoose.model('State', StateSchema);