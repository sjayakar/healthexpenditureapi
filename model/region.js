var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//need to update the schema
var RegionSchema   = new Schema({  
  Region_Number: Number,
  Region_Name: String
}

module.exports = mongoose.model('Region', RegionSchema);