'use strict';

var fs = require('fs'), 
  async = require('async'),
  mongoose = require('mongoose'),
  connection = require('./mangoprovider/connectionstring'),
	saveData = require('./loaddata/save-data'),
	config = require('./config/configuration'),
	data =  require('./dataconverter');
  
// Data
var cmsRawData = data;//JSON.parse(fs.readFileSync(config.jsonfilepath, 'utf8'));
 
 //console.log(cmsRawData);

// Connect to mongo
mongoose.connect(connection);

// Populate rows
async.forEachOf(cmsRawData, function(d, k, cb){
  saveData(d);
}, function(){
  console.log('done saving rows');
});