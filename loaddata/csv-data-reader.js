'use strict';

var fs = require('fs');

var csvdata =  function readdata (path) {	
	var filedata = fs.readFileSync(path ,'utf8');
	return filedata;
};
//console.log(csvdata('./loadData/US_AGGREGATE09_1.csv'));

module.exports = csvdata;