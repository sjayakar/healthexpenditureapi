'use strict';

var fs = require('fs');
var config = require('../Config/configuration');

 var csvarray =   function CSVToArray(strData, strDelimiter) {
    // Check to see if the delimiter is defined. If not,then default to comma.
    strDelimiter = (strDelimiter || ",");
    // Create a regular expression to parse the CSV values.
    var objPattern = new RegExp((
    // Delimiters.
    "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +
    // Quoted fields.
    "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +
    // Standard fields.
    "([^\"\\" + strDelimiter + "\\r\\n]*))"), "gi");
    
    var arrData = [[]];

    var arrMatches = null;
   
    while (arrMatches = objPattern.exec(strData)) {
        
        var strMatchedDelimiter = arrMatches[1];
        
        if (strMatchedDelimiter.length && (strMatchedDelimiter != strDelimiter)) {         
            arrData.push([]);
        }
        
        if (arrMatches[2]) {
            
            var strMatchedValue = arrMatches[2].replace(
            new RegExp("\"\"", "g"), "\"");
        } else {
            
            var strMatchedValue = arrMatches[3];
        }
       
        arrData[arrData.length - 1].push(strMatchedValue);
    }
  
    return (arrData);
}

var csvtojson = function CSV2JSON(csv) {
	var array = csvarray(csv);
  var objArray = [];
  for (var i = 1; i < array.length; i++) {
  	objArray[i - 1] = {};
    for (var k = 0; k < array[0].length && k < array[i].length; k++) {
      var key = array[0][k];
      objArray[i - 1][key] = array[i][k]
    }
  }

  var json = JSON.stringify(objArray);
  var str = json.replace(/},/g, "},\r\n");
  return str;
}


module.exports = csvtojson;