'use strict';

var ip = require('./ip')();

var dbconfig = {
  mongo_db: 'NaionaltHealthExpenditure',
  mongo_port: '27017',
  mongo_url: 'localhost'
};

var csvfilepath = './loadData/US_AGGREGATE09.csv',	
	jsonfilepath = './loadData/health-expenditure-data.json',	
	welcome_msg = 'Welcome to Health Expenditure API',	
	port = process.env.PORT || 8080;

var host = 'http://' + ip + ':' + port;

var routes = {
	api_url_health: '/healthapi',
	api_url_states : '/states',
	api_url_regions : '/regions',
	api_url_data : '/data'
};

module.exports = {
	dbconfig : dbconfig, 
	csvfilepath: csvfilepath,
	jsonfilepath: jsonfilepath,
	routes: routes, 
	welcome_msg: welcome_msg,
	host: host,
	port: port
};
